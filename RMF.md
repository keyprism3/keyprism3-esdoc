## RMF

`C:\_\keyprism3\._esdoc`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `3`
- `webthought_id` - `3`
- `thought_uid` - `2BB41626-69C9-90A6-727E-5D31F5E1244B`
- `vp_url` - `keyprism3.vpp://shape/4cdvCNqGAqAAAgtu/hi0fCNqGAqAAAguy`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/xNZ6PVqGAqAAAhy0`
- `FmColorName` - `red-dark`
- `ColorHex` - `#F43F2A`
- `ColorRgb` - `244,63,42`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc) _EsDoc Output for KeyPrism3_

[ ![Codeship Status for keyprism3/keyprism3-esdoc](https://app.codeship.com/projects/547f7550-ecc6-0134-e2b6-0a42fa094665/status?branch=master)](https://app.codeship.com/projects/208433)

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2F%34cdvCNqGAqAAAgtu%2Fhi%30fCNqGAqAAAguy) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2F%34cdvCNqGAqAAAgtu%2Fhi%30fCNqGAqAAAguy) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%32BB%34%31%36%32%36-%36%39C%39-%39%30A%36-%37%32%37E-%35D%33%31F%35E%31%32%34%34B) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-esdoc) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-3) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_4cdvCNqGAqAAAgtu.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `.git`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cast%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%35%26webthought_id%3D%31%36%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%39Z_uiNqGAqAAAg%30_%2FQGfeiNqGAqAAAg%32u%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%37pN%30iNqGAqAAAgxG) `ast` - json files (`name.js.json`) and directories containing abstract syntax trees
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cclass%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%36%26webthought_id%3D%31%36%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%33dJhiNqGAqAAAg%33s%2FqXlhiNqGAqAAAg%33%35%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FkQj%30iNqGAqAAAgxQ) `class` - `html` files and directories containing class documentation `path/filename.ext~classname.html`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%37%26webthought_id%3D%31%36%37%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FBWURiNqGAqAAAg%34U%2FUk%38RiNqGAqAAAg%34h%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%33%36L%30iNqGAqAAAgxa) `css` - CSS files for esdoc, additionals from `.assets...`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cfile%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%38%26webthought_id%3D%31%36%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2Fl%38WpiNqGAqAAAg%34%37%2FDvupiNqGAqAAAg%35I%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%32X%33%30iNqGAqAAAgxk) `file` - `html` files and directories containing highlighted code `path/filename.ext`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cfunction%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%39%26webthought_id%3D%31%36%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FySHpiNqGAqAAAg%35l%2FEE%33piNqGAqAAAg%35y%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fmo_%30iNqGAqAAAgxu) `function` - Function Index
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cimage%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%30%26webthought_id%3D%31%37%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FRK%36ZiNqGAqAAAg%36Q%2FUyOZiNqGAqAAAg%36d%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FjKoMiNqGAqAAAgx%38)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cimage%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%30%26webthought_id%3D%31%37%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FRK%36ZiNqGAqAAAg%36Q%2FUyOZiNqGAqAAAg%36d%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FjKoMiNqGAqAAAgx%38) `image` - Images for EsDoc
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%31%26webthought_id%3D%31%37%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FkHNZiNqGAqAAAg%36%33%2FCJDZiNqGAqAAAg%37E%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FEskMiNqGAqAAAgyG) `script` - EsDoc built-in scripts, and additional scripts plus `manual.js` custom script
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ctest-file%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%33%26webthought_id%3D%31%37%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%38P%39%35iNqGAqAAAg%38n%2FRiL%35iNqGAqAAAg%38%30%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F_N%38MiNqGAqAAAgyY) `test-file` - `html` files and directories containing highlighted code of test files `path/filename.test.ext`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cvariable%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%34%26webthought_id%3D%31%37%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FcTaFiNqGAqAAAg%39N%2FiFWFiNqGAqAAAg%39a%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FK%33aMiNqGAqAAAgym) `variable` - EsDoc Variable Index HTML

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cbadge.svg) `badge.svg` - documentation coverage badge
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccoverage.json) `coverage.json` - documentation coverage breakdown
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cdump.json) `dump.json` - array of objects with docId, kind, name, memberof, longname, access, description, lineNumber, params
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cidentifiers.html) `identifiers.html` - References page
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cindex.html) `index.html` - Main Page
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Csource.html) `source.html` - Source Page
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ctest.html) `test.html` - Test Page

