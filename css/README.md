## README

`C:\_\keyprism3\._esdoc\css`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `167`
- `thought_uid` - `C242FEAE-87BE-F0CE-9AE8-1635821B42D5`
- `vp_url` - `keyprism3.vpp://shape/BWURiNqGAqAAAg4U/Uk8RiNqGAqAAAg4h`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/36L0iNqGAqAAAgxa`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss) _CSS files for esdoc, additionals from `.assets...`_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%26webthought_id%3D%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%34cdvCNqGAqAAAgtu%2Fhi%30fCNqGAqAAAguy%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FxNZ%36PVqGAqAAAhy%30) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FBWURiNqGAqAAAg%34U%2FUk%38RiNqGAqAAAg%34h) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FBWURiNqGAqAAAg%34U%2FUk%38RiNqGAqAAAg%34h) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/C%32%34%32FEAE-%38%37BE-F%30CE-%39AE%38-%31%36%33%35%38%32%31B%34%32D%35) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-esdoc/src/master/css/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-167) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_BWURiNqGAqAAAg4U.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss%5CcontextMenu.css) `contextMenu.css` - for `contextMenu.min.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss%5Cjquery.qtip.min.css) `jquery.qtip.min.css` - for `jquery.qtip.min.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss%5Cprettify-tomorrow.css) `prettify-tomorrow.css` - for `prettify.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss%5Cstyle-qtip.css) `style-qtip.css` - for `jquery.qtip.min.js` - extending
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Ccss%5Cstyle.css) `style.css` - main EsDoc styles - in transition

