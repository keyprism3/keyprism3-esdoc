## README

`C:\_\keyprism3\._esdoc\script`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `171`
- `thought_uid` - `76105214-21B4-0284-AFAB-05902C4BB27F`
- `vp_url` - `keyprism3.vpp://shape/kHNZiNqGAqAAAg63/CJDZiNqGAqAAAg7E`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/EskMiNqGAqAAAgyG`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript) _EsDoc built-in scripts, and additional scripts plus `manual.js` custom script_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%26webthought_id%3D%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%34cdvCNqGAqAAAgtu%2Fhi%30fCNqGAqAAAguy%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FxNZ%36PVqGAqAAAhy%30) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FkHNZiNqGAqAAAg%36%33%2FCJDZiNqGAqAAAg%37E) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FkHNZiNqGAqAAAg%36%33%2FCJDZiNqGAqAAAg%37E) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%37%36%31%30%35%32%31%34-%32%31B%34-%30%32%38%34-AFAB-%30%35%39%30%32C%34BB%32%37F) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-esdoc/src/master/script/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-171) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_kHNZiNqGAqAAAg63.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cprettify%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%32%26webthought_id%3D%31%37%32%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FMH%32MiNqGAqAAAgyw) `prettify` - Prettify Code

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cclipboard.min.js) `clipboard.min.js` - Clipboard Copy
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5CcontextMenu.min.js) `contextMenu.min.js` - Context Menu
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cinherited-summary.js) `inherited-summary.js` - Toggle visibilit for `.inherited-summary`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cinner-link.js) `inner-link.js` - hash change, scroll window for `.inner-link-active`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cjquery-%32.%31.%34.min.js) `jquery-2.1.4.min.js` - EsDocs wants
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cjquery.qtip.min.js) `jquery.qtip.min.js` - QTip2 for jQuery
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5CloadCSS.js) `loadCSS.js` - Load CSS file asynch with window.loadCSS(href, before, media, idd)`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Clscache.min.js) `lscache.min.js` - localStorage extension [lscashe Github](https://github.com/pamelafox/lscache)
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cmanual.js) `manual.js` - Mark Robbins EsDoc Enhancements `window._`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cnotify.min.js) `notify.min.js` - [notifyjs.com](https://notifyjs.com/)
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5ConloadCSS.js) `onloadCSS.js` - adds onload support for asynchronous stylesheets loaded with `loadCSS.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cpatch-for-local.js) `patch-for-local.js` - change `./` hrefs to `./index.html` when protocol is `file:`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Cpretty-print.js) `pretty-print.js` - `location.hash` has `errorLines` set `error-line` class
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Csearch.js) `search.js` - EsDocs
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Csearch_index.js) `search_index.js` - EsDocs search data
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cscript%5Ctest-summary.js) `test-summary.js` - Toggles

