## README

`C:\_\keyprism3\._esdoc\ast`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `165`
- `thought_uid` - `4D0AEA24-19AA-F623-9D13-4E329ABD3629`
- `vp_url` - `keyprism3.vpp://shape/9Z_uiNqGAqAAAg0_/QGfeiNqGAqAAAg2u`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/7pN0iNqGAqAAAgxG`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cast) _json files (`name.js.json`) and directories containing abstract syntax trees_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%26webthought_id%3D%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%34cdvCNqGAqAAAgtu%2Fhi%30fCNqGAqAAAguy%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FxNZ%36PVqGAqAAAhy%30) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cast%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2F%39Z_uiNqGAqAAAg%30_%2FQGfeiNqGAqAAAg%32u) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2F%39Z_uiNqGAqAAAg%30_%2FQGfeiNqGAqAAAg%32u) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%34D%30AEA%32%34-%31%39AA-F%36%32%33-%39D%31%33-%34E%33%32%39ABD%33%36%32%39) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-esdoc/src/master/ast/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-165) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_9Z_uiNqGAqAAAg0_.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `source` - source code directory structure
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `test` - test code directory structure

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cast%5CREADME.html) `README.html`

