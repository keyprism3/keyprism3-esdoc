## RMF-file

`C:\_\keyprism3\._esdoc\file`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `168`
- `thought_uid` - `2DB61E97-1DBD-4231-0D9A-8072315F6C4F`
- `vp_url` - `keyprism3.vpp://shape/l8WpiNqGAqAAAg47/DvupiNqGAqAAAg5I`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/2X30iNqGAqAAAgxk`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cfile) _`html` files and directories containing highlighted code `path/filename.ext`_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%26webthought_id%3D%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%34cdvCNqGAqAAAgtu%2Fhi%30fCNqGAqAAAguy%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FxNZ%36PVqGAqAAAhy%30) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cfile%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2Fl%38WpiNqGAqAAAg%34%37%2FDvupiNqGAqAAAg%35I) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2Fl%38WpiNqGAqAAAg%34%37%2FDvupiNqGAqAAAg%35I) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%32DB%36%31E%39%37-%31DBD-%34%32%33%31-%30D%39A-%38%30%37%32%33%31%35F%36C%34F) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-esdoc/src/master/file/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-168) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_l8WpiNqGAqAAAg47.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `app` - application root directory

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5Cfile%5CREADME.html) `README.html`

